
import math
import matplotlib.pyplot as plt
import random
import copy

class Parser:
    def __init__(self):
        self.assignment = {}#initializes the string to float assignment dictionary
        self.colorList = ["red","green","blue","orange","purple","magenta","gray","black","navy"]
    
    def dotProduct(self,values,weights):
        return sum(value * weight for value, weight in zip(values, weights))#gets the dot product using the weights
    
    def convertStringToFloat(self, row):#converts strings such as iris-setosa to .14
        if row.find(",") !=-1:#permits the use of various delimiters for the input data
            stringvalues = row.strip().split(",")
        elif row.find("    ") !=-1:
            stringvalues = row.strip().split("    ")
        elif row.find(" ") !=-1:
            stringvalues = row.strip().split(" ")
        elif row.find("\t") !=-1:
            stringvalues = row.strip().split("\t")

        tmpRow = []
        for item in stringvalues:
            try:tmpRow.append(float(item))
            except:tmpRow.append(self.assignment[item])
        return tmpRow
    
    def shuffleAssignment(self):#randomizes the string to float parameters to find a better fit for classification
        tmplist = []
        for item in self.assignment:
            tmplist.append(self.assignment[item])
        i = 0   
        random.shuffle(tmplist)
        for item in self.assignment:
            self.assignment[item] = tmplist[i%len(self.assignment)]
            i += 1

    def generateAssignment(self):#splits 1 into x number of sections for classification criteria
        count = len(self.assignment)
        buffer = .00001
        bufferPlus = buffer * count
        splitVal = ((1.0-bufferPlus)/count)
        lowVal = 0.0
        self.colorAssignment = {}
        print "Assignment categories:",self.assignment
        for i,item in enumerate(self.assignment):
            highVal = splitVal + lowVal
            targetVal = lowVal + (splitVal/2.0)
            self.assignment[item] = [lowVal, highVal, targetVal]#creates the range for a match and the target value
            self.colorAssignment[item] = self.colorList[i%len(self.colorList)]
            lowVal = (highVal + buffer)
        self.shuffleAssignment()

    def splitDataSets(self, file):#splits the dataset into training and testing dataset
        self.trainingSet = []
        self.testingSet = []
        self.splitData = 0
        
        dataSet = open(file,"rb")
        for line in dataSet:
            self.splitData +=1
            
        self.splitData *= .75
        
        dataSet = open(file,"rb")
        for i, row in enumerate(dataSet):
            self.weights = [0] * ((len(row.split(",")))-1)#automatically initialize the number of zeroed out weights depending on the number of datapoints in the file
            if i < self.splitData:
                self.trainingSet.append(self.convertStringToFloat(row))
            else:                
                self.testingSet.append(self.convertStringToFloat(row))
#        print "Training data:", self.trainingSet
#        print "Testing data:", self.testingSet
        print "Assignment:", self.assignment
        
    def initAssignments(self, file):#initializes string to float assignments to 0
        dataSet = open(file,"rb")
        for row in dataSet:
            self.assignment[(row.split(",")[-1]).strip()] = 0
            
    def generateLearningValue(self):
        for line in self.trainingSet:
            maximum = max(line[:-1])
            if maximum > 100:
                self.learningValue = 10000000.0
            elif maximum > 10:
                self.learningValue = 1000000.0
            elif maximum > 1:
                self.learningValue = 100000.0
            elif maximum > .1:
                self.learningValue = 10000.0
            else:
                self.learningValue = 1000.0
            print "Learning value:",self.learningValue
            return
    
    def parseData(self,file):#performs randomized classification
        self.randomizeFile(file)
        self.initAssignments(file)
        self.generateAssignment()
        self.splitDataSets(file)
        self.generateLearningValue()
        
        for _ in xrange(10000):
            testweights = copy.deepcopy(self.weights)
            for _ in range(len(self.weights)):
                testweights[random.randint(0,len(self.weights)-1)] = random.randint(-10000,10000)/self.learningValue
            if self.testData(self.trainingSet,testweights)[0] > self.testData(self.trainingSet,self.weights)[0]:
                self.weights = copy.deepcopy(testweights)
                print "weights:",self.weights, "perc,error:",self.testData(self.trainingSet,self.weights)[:-1]
        if self.testData(self.trainingSet,self.weights)[0] > 70:
            print "training", self.testData(self.trainingSet,self.weights)[:-1]
            self.trainingColors = copy.deepcopy(self.colors)
            print "testing", self.testData(self.testingSet,self.weights)[:-1]
            self.testingColors = copy.deepcopy(self.colors)
            print "final weights",self.weights
            self.graph(self.testData(self.trainingSet,self.weights)[-1],self.testData(self.testingSet,self.weights)[-1])
            return 1
        return 0
               
    def graph(self, trainOutput, testOutput):#graphs the data
        fig = plt.figure(figsize=(10, 8))
        self.graphTrainingData(fig,trainOutput)
        self.graphTestingData(fig,testOutput)
        plt.draw()
        plt.show()
        
    def graphTrainingData(self,fig,output):
        y_data = [output]
        x_data = [range(len(output))]
        ax = fig.add_subplot(211)
        ax.scatter(x_data, y_data,color=self.trainingColors)
        ax.set_title("Training Data Scatter Plot")
        ax.set_xlabel("Element")
        ax.set_ylabel("Class")
        ax.grid()
        plt.ylim(-.25,1.25)
        for item in self.assignment:
            ax.annotate(item, xy=(0,self.assignment[item][0]), xytext=(len(output)-1, self.assignment[item][0]),arrowprops=dict(arrowstyle='-'), ha='center', va='center')
        
    def graphTestingData(self,fig,output):
        y_data = [output]
        x_data = [range(len(output))]
        ax = fig.add_subplot(212)
        ax.scatter(x_data, y_data, color=self.testingColors)
        ax.set_title("Testing Data Scatter Plot")
        ax.set_xlabel("Element")
        ax.set_ylabel("Class")
        ax.grid()
        plt.ylim(-.25,1.25)
        for item in self.assignment:
            ax.annotate(item, xy=(0,self.assignment[item][0]), xytext=(len(output)-1, self.assignment[item][0]),arrowprops=dict(arrowstyle='-'), ha='center', va='center')
    
    def lookupColor(self,item):
        return self.colorAssignment[item]
    
    def testData(self,inputData,weights):
        graphOutput = []
        count = 0.0
        correct = 0.0
        totalDiff = 0.0
        expResults=[]
        self.colors = []
        
        for line in inputData:
            inputValues = line[:-1]
            lowVal,highVal,expectedResult = line[-1]
            expResults.append(expectedResult)
            result = self.dotProduct(inputValues,weights)
            diff = math.fabs(result - expectedResult)
            totalDiff += diff
            if result <= highVal and result >= lowVal:
                correct+=1                
            count+=1
            graphOutput.append(self.dotProduct(inputValues,weights))
            
        percCorrect = (correct/count)*100
        
        for item in expResults:
            for value in self.assignment:
                if item == self.assignment[value][-1]:
                    self.colors.append(self.lookupColor(value))

        return percCorrect,totalDiff,graphOutput

    def randomizeFile(self,file):
        data = []
        origFile = open(file,"rb")
        for line in origFile:
            if not (line == "\n"):
                data.append(line)
        origFile.close()
        newFile = open(file,"wb")
        random.shuffle(data)
        newFile.writelines(data)
        newFile.close()

def main():
    while 1:
        instance.parseData("iris.data")
#        instance.parseData("test.csv")
#        instance.parseData("ecoli.csv")
#        instance.parseData("ionosphere.data")
     
instance = Parser()
main()